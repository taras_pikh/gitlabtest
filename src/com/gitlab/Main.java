package com.gitlab;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        System.out.println("hello world!!!");
        System.out.println(getTodayData());
    }

    public static String getTodayData() {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        return  formatter.format(new Date());
    }
}
